
'''
Created on Aug 29, 2012
 * Edge.py
 *
 * This is a class to model and manipulate an edge in a directed graph.
 *
@author: David B. Bunker
'''

class Edge(object):
   '''
   classdocs
   '''


   def __init__(self, vFrom, vTo, attr=bool(False)):
      '''
         * The constructor for an Edge object.  It takes
         * the origin Vertex (from), the target Vertex (to) and a boolean
         * attribute (attr) indicating whether or not the Edge is transverse.  Then
         * it makes an Edge object with those arguments in the fVertex, tVertex,
         * and eAttr fields.
         *
         * from = the origin Vertex
         * to = the target Vertex
         * attr = the boolean designation of whether the Edge is transverse

      '''

      self.fVertex = vFrom
      self.tVertex = vTo
      if attr == 'false': attr = bool(False)
      if attr == 'true': attr = bool(True)
      self.eAttr = attr
   
   '''
      * A method returning the origin Vertex (fVertex value)
   '''
   def getFrom(self):
      return self.fVertex
   
   '''
      * A method returning the target Vertex (tVertex value).
   '''
   def getTo(self):
      return self.tVertex
   
   '''
    * A method to set the eAttr value to the input boolean argument 'attr'.
    * This eAttr is true if the edge is transverse and false otherwise.
   '''
   def setAttr(self, attr):
      self.eAttr = attr
      
   '''
    * A method to get the eAttr value of the Edge.
    * This eAttr is true if the edge is transverse and false otherwise.
   '''
   def getAttr(self):
      return self.eAttr
