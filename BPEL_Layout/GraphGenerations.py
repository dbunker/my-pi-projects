'''
Created on Aug 31, 2012
 * GraphGenerations.py
 * This is actually more a tree generation marking class.  It assumes the
 * input graph is in a tree format and all part of a single connected
 * component.  This is not a bad assumption since it is to be used on a
 * tree made from and input BPEL (xml) file which causes the tree to have
 * a single root node from which the rest of the tree branches.

@author: David Bunker
'''
#import Graph

class GraphGenerations(object):
   '''
   classdocs
   '''


   def __init__(self, gr):
      '''
      Constructor
      '''
      self.g = gr
      self.gen = []
      self.genSum = []
      self.gens = 0
      self.genLists = {}
   
      vList = self.g.getVertices()
      i = 0
      for vt in vList:
         vt.setNumber(i)
         i += 1
      #origVList = self.g.getVertices()
      num = len(vList)
      print "num is:",
      print num
      print "The first vertex is:",
      print vList[0].getName()
      #self.gen = num
      #self.genSum = num
      for i in range(num):
         self.gen.append(-1)
         self.genSum.append(0)
      
      # initialize for first vertex   
      self.gen[0] = 0
      self.genSum[0] += 1
      
      pipe = []
      pipe.append(vList.pop(0))
      while pipe:
         v = pipe.pop(0)
         #print "In constructor while pipe loop:",
         #print v.getName()
         eList = v.getOutgoing()
         #print "eList size is:",
         #print len(eList)
         fromGen = self.gen[v.getNumber()]
         newGen = -1;
         for e in eList:
            if not e.getAttr():
               vIndex = self.g.indexOfVert(e.getTo())
               #print "vIndex =",
               #print vIndex
               if newGen != (fromGen + 1):
                  newGen = fromGen + 1
               self.gen[vIndex] = newGen
               self.genSum[newGen] += 1
               pipe.append(e.getTo())
      
      gens = 0
      for index in range(num):
         self.gens += 1
      
      for i in range(gens):
         gList = self.getGenListInts(i)
         gVList = self.genLists.pop(i)
         if gVList == None:
            gVList = []
            for j in gList:
               gVList.append(self.g.getVertexByIndex(j))
            self.genLists[i] = gVList
   
   def setGraph(self, gr):
      self.g = gr
            
   def getGens(self):
      return self.gens
   
   def getMaxGen(self):
      maxGen = 0
      for i in range(self.gens):
         if self.genSum[i] > self.genSum[maxGen]:
            maxGen = i
      return maxGen
   
   def getVertexNumber(self, thisGen):
      return self.genSum[thisGen]
   
   def getGenNumber(self, vert):
      return self.gen[self.g.getVertices().indexOf(vert)]
   
   def getGenList(self, genNumber):
      return self.genLists.pop(genNumber)
   
   def getGenListInts(self, genNumber):
      numInGen = self.genSum[genNumber]
      genList = numInGen
      vList = self.g.getVertices()
      j = 0
      for i in range(len(vList)):
         if self.getGenNumber(vList.pop(i)) == genNumber:
            genList[j] = i
            j +=1
      return genList
   
   def getNumInGen(self, genNumber):
      return self.genSum[genNumber]
   
   def commonAncestor(self, v, w):
      vGen = self.getGenNumber(v)
      wGen = self.getGenNumber(w)
      if (vGen > wGen):
         while (self.getGenNumber(v) > self.getGenNumber(w)):
            v = self.g.parentOf(v)
      elif (vGen < wGen):
         while (self.getGenNumber(v) < self.getGenNumber(w)):
            w = self.g.parentOf(w)
      while not ((v == w) | (self.getGenNumber(v) < 1)):
         v = self.g.parentOf(v)
         w = self.g.parentOf(w)
      return v
   

