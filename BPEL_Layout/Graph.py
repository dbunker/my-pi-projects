'''
Created on Aug 29, 2012
 * Graph.py
 *
 * This is a class to model and manipulate a directed graph.
 *
 * This class is modeled after the ADT in chapter 13 of "Data
 * Structures & Algorithms in Java", Fifth edition, by Michael T. Goodrich
 * and Roberto Tamassia.
 *
@author: David B. Bunker
'''

import Vertex
import Edge
import GraphGenerations


class Graph(object):
   '''
   classdocs
   '''


   def __init__(self):
      '''
      Constructor
      '''
      self.vertices = []
      self.edges = []
      
   '''
    * Returns an iterable collection of all the vertices of the graph.
   '''
   def getVertices(self):
      return self.vertices
   
   '''
    * Returns the index (in the vertices List) of a given Vertex in the Graph.
    *
    * v = the Vertex to which the returned index integer belongs.
   '''
   
   def indexOfVert(self, v):
      #print "trying to get a vertex",
      #print v.getName(),
      indV = self.getVertices().index(v)
      #print indV
      return indV
   
   
   '''
    * Returns an iterable collection of all the edges of the graph.
   '''
   def getEdges(self):
      return self.edges

   '''
    * Returns an iterable collection of the edges incident upon Vertex v.
    *
    * v = the Vertex to which the incident edges belong.
   '''
   def incidentEdges(self, v):
      self.eList = []
      self.eList.append(v.getIncoming)
      self.eList.append(v.getOutgoing)
      return self.eList
   
   '''
    * Return the end Vertex of edge e distinct from vertex v;
    * An error occurs if e is not incident on v.
    *
    *
    * v = The original Vertex
    *
    * e = The edge to be analyzed
   '''
   def opposite(self, v, e):
      if (e.getFrom() == v):
         return e.getTo()
      if (e.getTo() == v):
         return e.getFrom()
      return None
   
   '''
    * Returns a two member array storing the end vertices of edge e.
    *
    * e = The edge to be analyzed
   '''
   def endVertices(self, e):
      vList = []
      vList.append(e.getFrom())
      vList.append(e.getTo())
      return vList
   
   '''
    * A method to test whether vertices v and w are adjacent.  It returns
    * True if there is an Edge in the Graph connecting Vertex v and Vertex w.
    * It returns False if not.
   '''
   def areAdjacent(self, v, w):
      iEdges = self.incidentEdges(v)
      for e in iEdges[:]:
         if (w == self.opposite(v, e)):
            return bool(True)
      return bool(False)
   
   '''
    * This method replaces the element (a String) stored at Vertex v with x.
   '''
   def vReplace(self, v, x):
      v.setName(x)
      
   '''
    * This method replaces the element (a String) stored at Edge e with x.
   '''
   def eReplace(self, e, x):
      e.setAttr(x)
   
   '''
    * This method inserts a new Vertex, which has already been constructed
    * into the List 'vertices' in the Graph.
   '''
   def insertVertex(self, aVert):
      self.vertices.append(aVert)
      return aVert
   
   '''
    * This method inserts a new Vertex, which has not already been constructed
    * into the List 'vertices' in the Graph.
   '''
   def insertNewVertex(self, vName=None):
      V = Vertex.Vertex(self, vName)
      self.vertices.append(V)
      
   
   '''
    * This method inserts a new directed edge with end Vertices v and w
    * and storing the boolean element x as the Edge's attribute into the
    * List 'edges' in the Graph.  The element x is true if the edge is
    * transverse and false if it is a pathway edge.
    * It also returns a boolean value 'true'
    * if the insertion was successful or 'false' otherwise.
    *
    *
    * v = the origin Vertex for the Edge
    *
    * w = the destination Vertex for the Edge
    *
    * x = the boolean value for whether the edge is a transverse edge.
    *
    * return a boolean value denting the success of the insertion
   '''
   def insertEdge(self, v, w, x=bool(False)):
      if v == w:
         return bool(False)
      for e in v.getOutgoing():
         if (self.opposite(v, e) == w):
            return bool(False)
      aEdge = Edge.Edge(v, w, x)
      v.addToOutgoing(aEdge)
      w.addToIncoming(aEdge)
      self.edges.append(aEdge)
      return bool(True)
   
   '''
    * A method to return the first vertex with the name attribute which
    * is given by the nm variable.  This is not as safe as getVertexByIndex
    * if you want to be sure the Vertex is unique.
    *
    * nm = the name value of the Vertex you are seeking.
    * return the vertex with the name given by nm.
   '''
   def getVertexByName(self, nm):
      # j = False
      # vName = None
      for v in self.vertices:
         if (v.getName() == nm):
            return v
   
   '''
    * A method to return the vertex with the index given by the ind variable.
    * ind = the index of the Vertex you are seeking.
    * return the vertex with the name given by ind index.
   '''
   def getVertByIndex(self, ind):
      return self.vertices.__getitem__(ind)
   
   '''
    * A method to remove Vertex V from the Graphs List 'vertices'.
    *
    * v = the Vertex to be removed.
   '''
   def removeVertex(self, v):
      eList = self.incidentEdges(v)
      for e in eList:
         self.removeEdge(e)
      self.vertices().remove(v)
      
   '''
    * A method to remove Edge E from the Graphs List 'edges'.
    *
    * e = the Edge to be removed.
   '''
   def removeEdge(self, e):
      eAttr = e.getAttr()
      v = e.getFrom()
      w = e.getTo()
      v.removeEdge(e)
      w.removeEdge(e)
      self.edges.remove(e)
      return eAttr
   
   '''
    * This method returns the Vertex which is the parent of the input
    * Vertex v.
    *
    * It assumes there is only one pathway (non-transverse) Edge in the
    * incoming() list for the Vertex v.  This is a safe assumption for the
    * data this program will be using.
    *
    * v = the Vertex whose parent this method finds
    * return the parent of Vertex v
   '''
   def parentOf(self, v):
      eList = v.getIncoming()
      vOut = None
      for e in eList:
         if not e.getAttr():
            vOut = e.getFrom()
      return vOut
   
   '''
    * This method returns a list of the Vertices which are the children of the input
    * Vertex v.
    *
    * v = the Vertex whose children this method finds
    * return a list of the children of Vertex v
   '''
   def childrenOf(self, v):
      children = []
      eList = v.getOutgoing()
      for e in eList:
         if not e.getAttr():
            children.append(e.getTo())
      return children
   
   '''
    * This method returns a list of the Vertices which are in the same generation as
    * Vertex v.
    *
    * v = the Vertex whose siblings this method finds
    * return a list of the siblings of Vertex v
   '''
   def findSiblings(self, v):
      w = self.parentOf(v)
      siblings = self.childrenOf(w)
      ind = siblings.index(v)
      del siblings[ind]
      return siblings
      
   '''
    * A method to build a graph by scanning in the members from a given
    * list (temporarily, the list is D:\Dave\charInput.txt).  The input
    * file is a text file of the format: number of vertices, number of
    * edges, names of all the vertices, a list of the edges with each
    * entry being entries in the list in the order origin vertex, destination
    * vertex, boolean value or whether the edge is transverse.
    *
    * As currently written, this method makes two assumptions.  The first is
    * that the first scanned Vertex is the root.  The second assumption is that
    * the whole Graph (Tree) is made up of a single component.  Since this
    * package is being written for BPEL processes and that if the BPEL process
    * is written correctly these assumptions are correct, they should be
    * satisfactory unless the package is used for something else, at which time
    * it will have to be modified.
    '''
   def scan(self):
      # vList = self.getVertices()
      # G = self
      inputFile = open("D:\\Dave\\charInput.txt", 'r')
      Vert = int(inputFile.readline())
      Ed = int(inputFile.readline())
      for i in range(Vert):
         vertName = inputFile.readline()[:-1]
         self.insertNewVertex(vertName)
      for i in range(Ed):
         v = int(inputFile.readline()[:-1])
         vIns = self.getVertByIndex(v)
         w = int(inputFile.readline()[:-1])
         wIns = self.getVertByIndex(w)
         x = inputFile.readline()[:-1]
         if x == "true": x = bool("True")
         if x == "False": x = bool("False")
         self.insertEdge(vIns, wIns, x)
   
   def show(self):
      vList = self.getVertices()
      for v in vList:
         ind = vList.index(v)
         print str(ind) + ":",
         aList = v.getOutgoing()
         for e in aList:
            tV = e.getTo()
            num = vList.index(tV)
            print num,
         print

   def showName(self):
      vList = self.getVertices()
      for v in vList:
         print v.getName() + ":",
         aList = v.getOutgoing()
         for e in aList:
            tV = e.getTo()
            name = tV.getName()
            print name,
         print

   
   '''
    * a method to rearrange the vertices in the Graph object's vertices field.
    * The rearrangement will order the vertices in such a way that sibling
    * vertices are adjacent to each other.
   '''
   def rearrangeVertices(self):
      G2 = Graph()
      V = len(self.vertices)
      for i in range(V):
         G2.insertNewVertex()
      vQue = []
      v2Que = []
      eList = []
      trans = {}
      vert0 = self.getVertByIndex(0)
      G2.getVertByIndex(0).setName(vert0.getName())
      vQue.append(vert0)
      v2Que.append(G2.getVertByIndex(0))
      j = 1
      while vQue:
         # print "You are in the while vQue loop!"
         v = vQue.pop(0)
         v2 = v2Que.pop(0)
         for e in v.getOutgoing():
            # add the correct name to the e.to() Vertex
            eAttr = e.getAttr()
            # print "eAttr is ",
            # print eAttr
            if eAttr == bool(False):
               # print "You are here"
               v2To = G2.getVertByIndex(j)
               j += 1
               trans[self.indexOfVert(e.getTo())] = v2To
               v2To.setName(e.getTo().getName())
               G2.insertEdge(v2, v2To)
               # print "adding " + e.getTo().getName() + " to vQue"
               vQue.append(e.getTo())
               # print "adding " + v2To.getName() + " to v2Que"
               v2Que.append(v2To)
            if eAttr == bool(True):
               eList.append(e)
      for e in eList:
         vFrom = trans[self.indexOfVert(e.getFrom())]
         vTo = trans[self.indexOfVert(e.getTo())]
         G2.insertEdge(vFrom, vTo, bool(True))
      return G2
   



if __name__ == '__main__':
   import TestStructure
   TestStructure.TestStructure()
   