'''
 * Vertex.py
 *
 * This is a class to model and manipulate a vertex in a directed graph.
 *
Created on Aug 29, 2012

@author: David B. Bunker
'''

#  The Vertex class creates and manipulates vertex objects for a Graph.

class Vertex(object):
   '''
   classdocs
   '''
   
   def __init__(self, g, name=None, data=None):
      '''
      Constructor
      '''
      self.incomingEdges = []
      self.outgoingEdges = []
      self.vName = name
      self.vAttr = data
      '''Add a Vertex number or use the data attribute for it!'''
      self.number = 0
      # G = g

   '''
   * A method to set the vertex's name (vName variable).
   *
   * n = the name to set the vName variable to.
   '''
   def setName(self, n):
      self.vName = n

   '''
   * A method to get the vertex's name (vName variable).
   '''
   def getName(self):
      return self.vName

   '''
   * A method to set the vertex's attribute (vAttr variable).
   *
   * n = the attribute to set the vAttr variable to.
   '''
   def setAttr(self,data):
      self.vAttr = data

   '''
   * A method to get the vertex's attribute (vAttr variable)
   '''
   def getAttr(self):
      return self.vAttr
   
   def setNumber(self, n):
      self.number = n
   
   def getNumber(self):
      return self.number

   '''
   * A method to get the list of outgoing Edges
   '''
   def getOutgoing(self):
      return self.outgoingEdges

   '''
   * A method to add Edge e to the incomingEdges list for the Vertex.  If
   * the target Vertex of the Edge e is not the current Vertex object, the
   * edge will not be added and a boolean false will be returned.
   *
   * e = the edge to be added to the incomingEdges list
   * return true if the edge is added, false if the target edge is not 'self'
   '''
   def addToIncoming(self, e):
      if (e.getTo() != self):
         return False
      self.incomingEdges.append(e)
      return True

   '''
   * A method to add Edge e to the outgoingEdges list for the Vertex.  If
   * the origin Vertex of the Edge e is not the current Vertex object, the
   * edge will not be added and a boolean false will be returned.
   *
   * e = the edge to be added to the outgoingEdges List.
   * return true if the edge is added, false if the origin edge is not 'self'
   '''
   def addToOutgoing(self, e):
      if (e.getFrom() != self):
         return False
      self.outgoingEdges.append(e)
      return True

   '''
   * A method to remove Edge e from the Vertex's incoming or outgoing Edge
   * list (as appropriate).
   *
   * e = the Edge to be removed
   '''
   def removeEdge(self, e):
      if (e in self.incomingEdges):
         self.incomingEdges.remove(e)
      if ( e in self.outgoingEdges):
         self.outgoingEdges.remove(e)
         
 